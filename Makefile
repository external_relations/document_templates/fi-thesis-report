TEXS=$(wildcard *.tex)
PDFS=$(TEXS:.tex=.pdf)

all: $(PDFS) clean

clean:
	rm -f *.aux *.dvi *.log *.synctex.gz *.out *.nav *.toc *.snm *.run.xml *-blx.bib *.bbl *.blg *.bcf *.fls *.fdb_latexmk

%.pdf : %.tex fi-thesis-report.cls
	pdflatex $<
	pdflatex $<

.PHONY: all clean
